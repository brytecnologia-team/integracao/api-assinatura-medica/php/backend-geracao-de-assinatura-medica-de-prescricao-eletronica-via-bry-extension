<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AssinadorPdfController extends Controller
{

    // Token Authorization de autenticação gerado no BRy Cloud.
    const token = "";
    // URLs de inicialização e finalização de assinaturas.
    const urlInicializacao = "https://hub2.bry.com.br/fw/v1/pdf/pkcs1/assinaturas/acoes/inicializar";
    const urlFinalizacao   = "https://hub2.bry.com.br/fw/v1/pdf/pkcs1/assinaturas/acoes/finalizar";

    // Função chamada na inicialização da assinatura (Recebe como parametro a requisição enviada pelo front)
    public function inicializar(Request $request)
    {
        // Verifica se o documento enviado na requisição é válido.
        if ($request->hasFile('documento') && $request->file('documento')->isValid()) {
            $upload = $request->documento->storeAs('documentos', 'documentoParaAssinatura.pdf');
        } else {
            return response()->json(['message' => 'Arquivo enviado inválido'], 400);
        }
        // Verifica se a imagem enviada na requsição é valida.
        if ($request->incluirIMG === 'true') {
            if ($request->hasFile('imagem') && $request->file('imagem')->isValid()) {
                $upload = $request->imagem->storeAs('imagem', 'imagemAssinatura.png');
            } else {
                return response()->json(['message' => 'Imagem enviada inválida'], 400);
            }
        }

        $data = array(
            'dados_inicializar' =>
            '{
            "perfil" : "' . $request->perfil . '",
            "algoritmoHash" : "' . $request->algoritmoHash . '",
            "formatoDadosEntrada" : "Base64",
            "formatoDadosSaida" : "Base64",
            "certificado" : "' . $request->certificado . '",
            "nonces": [""]
        }',
            'documento' => new \CURLFILE(storage_path() . '/app/documentos/documentoParaAssinatura.pdf'),
    );
        if($request->assinaturaVisivel === 'true') {
            $data['configuracao_imagem'] = '{
                "altura" : "' . $request->altura . '",
                "largura" : "' . $request->largura . '",
                "coordenadaX" : "' . $request->coordenadaX . '",
                "coordenadaY" : "' . $request->coordenadaY . '",
                "posicao" : "' . $request->posicao . '",
                "pagina" : "' . $request->pagina . '"
            }';

            $data['configuracao_texto'] = '{
                "texto" : "' . $request->texto . '",
                "incluirCN" : "' . $request->incluirCN . '",
                "incluirCPF" : "' . $request->incluirCPF . '",
                "incluirEmail" : "' . $request->incluirEmail . '"
            }';

            // Configurações da imagem da assinatura
            if ($request->incluirIMG === 'true') {
                $data['imagem'] = new \CURLFILE(storage_path() . '/app/imagem/imagemAssinatura.png');
            };
        }

            // Verifica se o profissional informado é médico e, caso seja, insere também as OIDs do farmacêutico
            if ($request->numeroOID === "2.16.76.1.4.2.2.1") {
                $data['metadados'] = '{
                "' . $request->tipoDocumento . '" : "",
                "' . $request->UFOID . '" : "' . $request->UF . '",
                "' . $request->numeroOID . '" : "' . $request->numero . '",
                "' . $request->especialidadeOID . '" : "' . $request->especialidade . '",
                "2.16.76.1.4.2.3.1" : "",
                "2.16.76.1.4.2.3.2" : "",
                "2.16.76.1.4.2.3.3" : ""
                }';
            } else {
                $data['metadados'] = '{
                    "' . $request->tipoDocumento . '" : "",
                    "' . $request->numeroOID . '" : "' . $request->numero . '",
                    "' . $request->UFOID . '" : "' . $request->UF . '",
                    "' . $request->especialidadeOID . '" : "' . $request->especialidade . '"
                    }';
            }

        // Cria o CURL que será enviada à API de Assinatura.
        $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => self::urlInicializacao,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $data,
                CURLOPT_HTTPHEADER => array(
                    'Authorization: ' . self::token,
                ),
            ));

        // Envia a requisição para a API de Assinatura BRy
        $respostaInicializacao = curl_exec($curl);
        // Recebe o código HTTP da requisição
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        // Transforma a resposta da requisição em JSON
        $respostaJson = json_decode($respostaInicializacao);
        // Encerra a requisição
        curl_close($curl);

        // Se a requisição retornou o código HTTP 200, ou seja, sucesso, ele retorna 200 para o front.
        // Caso não tenha retornado 200, retorna a resposta vindo da API e o código http de erro para o front.
        // A resposta da inicialização será usada no frontend para cifrar os dados usando a BRy Extension.
        if ($httpcode == 200) {
            return response()->json($respostaJson);
        } else {
            return response()->json($respostaJson, $httpcode);
        }
    }

    // Função chamada na finalização da assinatura (Recebe como parametro a requisição enviada pelo front)
    public function finalizar(Request $request)
    {

        // // Cria o CURL que será enviada à API de Assinatura.
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => self::urlFinalizacao,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS =>
            "{
                \"nonce\": \"$request->nonce\",
                \"formatoDeDados\": \"Base64\",
                \"assinaturasPkcs1\":
                    [
                        {
                            \"cifrado\": \"$request->cifrado\",
                            \"nonce\": \"$request->insideNonce\"
                        }
                    ]
            }",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
            ),
        ));
        // Envia a requisição para a API de Assinatura BRy.
        $respostaFinalizacao = curl_exec($curl);
        // Recebe o código HTTP da requisição.
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        // Transforma a resposta da requisição em JSON.
        $respostaJson = json_decode($respostaFinalizacao);
        // Encerra a requisição.
        curl_close($curl);

        // // Se a requisição retornou o código HTTP 200, ou seja, sucesso, ele retorna 200 para o front junto
        // // com o link para download do diploma assinado.
        // // Caso não tenha retornado 200, retorna a resposta vindo da API e o código http de erro para o front.
        // return response()->json($respostaJson);
        if ($httpcode == 200) {
            return response()->json($respostaJson);
        } else {
            return response()->json($respostaJson, $httpcode);
        }
    }
}
